import React, { Component } from 'react'
import { connect } from 'react-redux'
import bindActionsToDispatch from 'helpers/bindActionsToDispatch'
import generalActions from 'redux/actions/generalActions'
import FormExercise from 'components/form/form-exercise/form-exercise'

import './home.css'

class Home extends Component {

	componentWillMount() {
		this.props.actions.initializeHomeData()
	}

	componentDidMount() {
	}

	render() {
		return (
			<div className='div-container-wrapper'>
				<FormExercise />
			</div>
		)
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionsToDispatch({
		initializeHomeData: generalActions.initializeHomeData
	}, dispatch)
}

function mapStateToProps(state) {
    return {
        isOpen: state
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)