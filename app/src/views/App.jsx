import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Redirect, Switch, withRouter } from 'react-router-dom'
import Async from 'react-code-splitting'
import './App.css'
import 'scss/global.css'

const home = (props) => <Async load={import('views/home/home')} componentProps={props} />

class App extends Component {
	render() {
		return (
			<div className='div-app-wrapper'>
				<Switch>
					<Route path={'/home'} component={home} />
					<Redirect to="/home" />
				</Switch>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return state
}

export default withRouter(connect(mapStateToProps, null)(App))
