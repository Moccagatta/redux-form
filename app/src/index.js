import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import history from 'helpers/history'
import store from 'redux/store'
import { Router } from 'react-router-dom'
import App from 'views/App'
import 'bootstrap/dist/css/bootstrap.min.css'

ReactDOM.render(
	<Provider store={store}>
		<Router history={history}>
			<App />
		</Router>
	</Provider>
	, document.getElementById('root'))