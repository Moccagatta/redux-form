import { call, takeEvery, takeLatest, put, select } from 'redux-saga/effects'
import actionTypes from '../actions/actionTypes'
import contentActions from '../actions/contentActions'
import formGetData from 'redux/selectors/form-exercise/getFormDataSelector'

function *initializeContent(action) {
	try {
		const response = yield call(fetch, 'api/content', {credentials: 'same-origin'})
		const data = yield call([response, response.json])

		if(data && data.statusCode === 200) {
			//yield put(contentActions.loadData(data.data.contentView))
		} else {
			alert('Problema con el servidor, inténtelo nuevamente más tarde...')
		}
	} catch (e) {
		console.log(e);
	}
}

function *sendContent(action) {
	try {
		const contentData = yield select(formGetData)
		const response = yield call(fetch, 'api/content', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin',
			body: JSON.stringify(contentData)
		})
		const data = yield call([response, response.json])

		if(data && data.statusCode === 200) {
//			yield put(contentActions.contentSendDataSuccess())
		} else {
			alert('Problema con el servidor, inténtelo nuevamente más tarde...')
		}
	} catch (e) {
		alert('Problema con el servidor, inténtelo nuevamente más tarde...')
	}	
}

export default [
	//takeLatest(actionTypes.DASHBOARD_INITIALIZE_DATA, initializeContent),
	//takeLatest([actionTypes.CONTENTVIEW_SEND_DATA, actionTypes.CONTENTVIEW_DELETE_CONTENT], sendContent)
]