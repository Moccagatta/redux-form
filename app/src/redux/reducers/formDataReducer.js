import actionType from '../actions/actionTypes'

let defaultState = {
	content: []
}

function addUser(state, action) {
	let newState = {...state}
	return newState
}

let formDataReducer = (state = defaultState, action) => {
	switch (action.type) {
		case actionType.USER_ADD_USER:
			return addUser(state, action)
		default:
			return state
	}
}

export default formDataReducer