import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import formDataReducer from './formDataReducer'

const rootReducer = combineReducers({
	routing: routerReducer,
	formData: formDataReducer
})

export default rootReducer
