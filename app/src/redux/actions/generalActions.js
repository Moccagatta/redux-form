import actionTypes from './actionTypes'

let generalActions = {
	initializeHomeData: function() {
		return {
			type: actionTypes.HOME_INITIALIZE_DATA
		}
	}
}

export default generalActions
