const actionTypes = {
	HOME_INITIALIZE_DATA: 	'HOME/INITIALIZE_DATA',
	
	USER_ADD_USER: 			'USER/ADD_USER'
}

export default actionTypes
