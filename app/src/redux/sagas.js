import form_sagas from './sagas/form_sagas'

export default function* rootSaga() {
	yield [
		form_sagas
	]
}
