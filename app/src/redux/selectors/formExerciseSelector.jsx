import { createSelector } from 'reselect'

const getContentData = (state) => state.content

const contentViewDataSelector = createSelector(
	[getContentData],
	(contentData) => {
		let content = []

		return {
			content
		}
	}
)

export default contentViewDataSelector