import React, { Component } from 'react'
import { connect } from 'react-redux'
import bindActionsToDispatch from 'helpers/bindActionsToDispatch'
import contentActions from 'redux/actions/contentActions'
import generalActions from 'redux/actions/generalActions'
import formExerciseDataSelector from 'redux/selectors/formExerciseSelector'

import './form-exercise.css'

class FormExercise extends Component {

	constructor(props) {
		super(props)
		this.state = {

		}
	}

	render() {
		return (
			<div className='div-wrapper-form-exercise'>
				Hello World
			</div>
		)
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionsToDispatch({
	}, dispatch)
}

function mapStateToProps(state) {
	return formExerciseDataSelector(state)
}

export default connect(mapStateToProps, mapDispatchToProps)(FormExercise)